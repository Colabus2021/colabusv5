package com.colabus.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ColabusApplication {

	public static void main(String[] args) {
		SpringApplication.run(ColabusApplication.class, args);
	}

}
